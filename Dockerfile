# https://github.com/joyzoursky/docker-python-chromedriver

FROM joyzoursky/python-chromedriver:3.8

# https://github.com/joyzoursky/docker-python-chromedriver/blob/master/py-debian/3.8/Dockerfile

# Define Workdir
WORKDIR /app

# Add user
RUN useradd -m -r user && \
    chown user /app

COPY requirements.txt /app/

RUN pip install --trusted-host pypi.python.org --requirement requirements.txt

COPY . /app

USER user

CMD ["python", "src/loop_check.py"]
