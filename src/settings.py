import logging
import os

from dotenv import load_dotenv
from rich.logging import RichHandler

load_dotenv()

SENDER_EMAIL = os.environ.get("SENDER_EMAIL")
SENDER_ALIAS_NAME = os.environ.get("SENDER_ALIAS_NAME")
SENDER_PWD = os.environ.get("SENDER_PWD")

alert_mode = os.environ.get("AlERT_MODE", "notification")
ifttt_webhook_url = os.environ.get("IFTTT_WEBHOOK_URL")

check_url = "https://pprdv.interieur.gouv.fr/booking/create/989/1"

email_to = os.environ.get("SEND_TO")
email_subject = "Check Quickly"
email_body = "Link: https://pprdv.interieur.gouv.fr/booking/create/989/1"

wait_sec = 30
max_try_times = 2

dev_mode = True if os.environ.get("DEV_MODE") == "true" else False

if dev_mode:
    log_level = logging.DEBUG
else:
    log_level = logging.INFO

maximum_logger_message_length = 200

if isinstance(maximum_logger_message_length, int):
    log_format = f"%(message).{maximum_logger_message_length}s"
else:
    log_format = "%(message)s"

DATE_STRFTIME_FORMAT = "%Y-%m-%d %H:%M:%S"

logging.basicConfig(
    level=log_level,
    format=log_format,
    datefmt=DATE_STRFTIME_FORMAT,
    handlers=[RichHandler()],
)
logger = logging.getLogger("rich")
