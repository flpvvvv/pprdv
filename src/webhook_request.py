from datetime import datetime

import requests

from settings import DATE_STRFTIME_FORMAT, ifttt_webhook_url


def send_request(url: str, slot_datetime: datetime):
    payload = {"value1": slot_datetime.strftime(DATE_STRFTIME_FORMAT)}
    headers = {}
    res = requests.post(url, data=payload, headers=headers)
    return res


if __name__ == "__main__":
    print("Test sending webhook request.")
    slot_datetime = datetime.now()
    send_request(ifttt_webhook_url, slot_datetime)
    print("Request sent.")
