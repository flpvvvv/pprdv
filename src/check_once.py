import logging
from datetime import datetime
from typing import Tuple

import dateparser
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from webdriver_manager.chrome import ChromeDriverManager

from settings import check_url, dev_mode, logger

# TODO: Unable to start a new session with 'headless' option !!
# TODO: Unable to locate recaptcha checkbox !!


def parser_datetime(appointment_string: str) -> datetime:
    """
    appointment_string example:
    ---
    Nature
    Remise de titre guichets 1,2 et 3
    Date
    lundi 06 décembre 2021
    Plage horaire
    11:50 à 11:55
    ---
    """
    appointment_li = appointment_string.split("\n")
    selected_date = appointment_li[3]
    start_time = appointment_li[5].split(" ")[0]
    datetime_str = selected_date + ", " + start_time
    appointment_datetime = dateparser.parse(datetime_str)
    # print(appointment_datetime)
    return appointment_datetime


def get_driver(log_level=logging.INFO) -> webdriver:
    caps = DesiredCapabilities.CHROME
    options = Options()
    # options.headless = True  # headless
    options.add_argument("--headless")
    options.add_argument("--no-sandbox")
    options.add_argument("--disable-dev-shm-usage")

    driver = webdriver.Chrome(
        ChromeDriverManager(
            print_first_line=False, log_level=log_level
        ).install(),
        options=options,
        desired_capabilities=caps,
    )
    return driver


def check_once(
    url: str,
) -> Tuple[datetime, str]:
    first_slot = datetime.fromisoformat("1970-01-01 00:00")
    appointment_string = ""

    if dev_mode:
        log_level = logging.INFO
    else:
        log_level = logging.WARNING

    try:
        driver = get_driver(log_level)
    except Exception as e:
        logger.error(e)
        logger.error("Unable to get webdriver.")
        raise e

    try:
        # caps = DesiredCapabilities.CHROME
        # driver.start_session(capabilities=caps)
        driver.get(url)

        # check if forbidden
        html_source = driver.page_source
        if "403 Forbidden" in html_source:
            appointment_string = "forbidden"
            logger.warning("The access has been forbidden!")
            logger.debug(html_source)
        else:
            # Click to choose "Remise de titre guichets 1,2 et 3"
            driver.find_element_by_id("planning990").click()
            # scroll down to the bottom
            driver.execute_script(
                "window.scrollTo(0, document.body.scrollHeight);"
            )
            # Click on "Next" button
            driver.find_element_by_name("nextButton").click()
            # Get booking form
            booking_form = driver.find_element_by_id("FormBookingCreate").text
            logger.debug(booking_form)

            # If there are available appointments
            # Postive msg: 'Vous avez choisi de prendre rendez-vous
            # pour effectuer la démarche suivante :
            # "Remise de titre guichets 1,2 et 3"\n
            # Chargement en cours...\n
            # Rechargement automatique du calendrier dans 30   seconde(s).'
            # Negative msg: "Il n'existe plus de plage horaire libre
            # pour votre demande de rendez-vous."
            if "Vous avez choisi de prendre rendez-vous" in booking_form:
                # scroll down to the bottom
                driver.execute_script(
                    "window.scrollTo(0, document.body.scrollHeight);"
                )
                # Click on "Next" button to choose the "earlist appointment"
                driver.find_element_by_name("nextButton").click()

                # Get appointment info
                appointment_string = driver.find_element_by_tag_name(
                    "fieldset"
                ).text
                first_slot = parser_datetime(appointment_string)

    except Exception as e:
        logger.debug(e)

    finally:
        driver.close()
        del driver

    return first_slot, appointment_string


if __name__ == "__main__":
    if dev_mode:
        logger.info("Dev mode")
    else:
        logger.info("Prod mode")
    # driver = get_driver()
    logger.info(check_once(check_url))
