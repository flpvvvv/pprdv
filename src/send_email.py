import yagmail

from settings import (
    SENDER_ALIAS_NAME,
    SENDER_EMAIL,
    SENDER_PWD,
    email_body,
    email_subject,
    email_to,
)


def send_email(to: str, contents: str = ""):
    if len(contents) == 0:
        contents = email_body
    else:
        contents += "\n" + email_body
    yag = yagmail.SMTP({SENDER_EMAIL: SENDER_ALIAS_NAME}, SENDER_PWD)
    yag.send(to=to, subject=email_subject, contents=contents)


if __name__ == "__main__":
    print("Test sending email.")
    contents = (
        "Nature\nRemise de titre guichets 1,2 et 3\n"
        "Date\nlundi 06 décembre 2021\nPlage horaire\n11:50 à 11:55"
    )
    send_email(email_to, contents)
    print("Email sent.")
