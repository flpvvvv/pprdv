import argparse
from datetime import datetime
from time import sleep

import dateparser

from check_once import check_once
from send_email import send_email
from settings import (
    alert_mode,
    check_url,
    dev_mode,
    email_to,
    ifttt_webhook_url,
    logger,
    max_try_times,
    wait_sec,
)
from webhook_request import send_request

if __name__ == "__main__":

    parser = argparse.ArgumentParser(
        description="Check if there is any appointment at the Préfecture."
    )

    parser.add_argument(
        "-w",
        "--wait_seconds",
        help="Wait N seconds between each query.",
        type=int,
        default=wait_sec,
    )
    parser.add_argument(
        "-m",
        "--max_attempt",
        help="Maximum attempt times.",
        type=int,
        default=max_try_times,
    )
    parser.add_argument(
        "-a",
        "--alert",
        help=(
            'The alert way, ["notification"|"email"], '
            'default is "notification"'
        ),
        type=str,
        default=alert_mode,
    )
    parser.add_argument(
        "-u",
        "--url",
        help="IFTTT webhook url",
        type=str,
        default=ifttt_webhook_url,
    )
    parser.add_argument(
        "-e",
        "--to_email",
        help="The email destination for alerts.",
        type=str,
        default=email_to,
    )
    parser.add_argument(
        "-b",
        "--before_datetime",
        help="Only alert for an early appointment. For ex: 2021-11-15 20:27",
        type=str,
        default="3000-01-01",
    )
    args = parser.parse_args()

    # check alert option
    if args.alert not in ["notification", "email"]:
        logger.warning(
            f'Alert mode "{args.alert}"" is not defined. '
            f'"notification" mode will be used.'
        )
        args.alert = "notification"

    mode_str = "Dev" if dev_mode else "Prod"

    logger.info(
        f"{mode_str} mode: {args.max_attempt} attempts "
        f"with {args.wait_seconds} seconds of interval. "
        f'Alert way: by "{args.alert}".'
    )

    before_datetime = dateparser.parse(args.before_datetime)

    try_times = 0
    first_available_slot = datetime.fromisoformat("2000-01-01")

    while True:
        first_slot, appointment_string = check_once(check_url)
        if appointment_string == "forbidden":
            logger.warning("The loop is stopped.")
            break
        logger.debug(first_slot)
        if len(appointment_string) > 0:
            logger.info(appointment_string)
        if first_available_slot < first_slot < before_datetime:
            # alert_way = ["notification"|"email"]
            if args.alert == "notification":
                # send a post request to IFTTT webhook
                send_request(args.url, first_slot)
            elif args.alert == "email":
                # send email
                send_email(to=args.to_email, contents=appointment_string)

            logger.info("CHECK !!")
            break
        else:
            try_times += 1
            logger.info(f"Try time: {try_times}/{args.max_attempt}")

        if try_times >= args.max_attempt:
            logger.info(
                f"After {try_times} attempts, there is still no appointment."
            )
            break

        # wait for X sec
        sleep(args.wait_seconds)
