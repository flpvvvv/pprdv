# pprdv

Check if there is any appointment at the Préfecture site: https://pprdv.interieur.gouv.fr/booking/create/989/.

## Versions

### V3.0

Changelog:

- Use [IFTTT](https://ifttt.com/) to send notification

Setup a webhook to receive a web request, then send a notification from the IFTTT app.

When an appointment is detected, the app will send a web request, instead of sending an email.
For example:
```bash
curl -X POST -H "Content-Type: application/json" -d "{\"value1\":\"<appointment_date_time>\"}" https://maker.ifttt.com/trigger/pprdv/with/key/<token>
```

- Add "notification" as default alert way

`loop_check` function arguments:

```
usage: loop_check.py [-h] [-w WAIT_SECONDS] [-m MAX_ATTEMPT] [-a ALERT] [-u URL] [-e TO_EMAIL] [-b BEFORE_DATETIME]

Check if there is any appointment at the Préfecture.

optional arguments:
  -h, --help            show this help message and exit
  -w WAIT_SECONDS, --wait_seconds WAIT_SECONDS
                        Wait N seconds between each query.
  -m MAX_ATTEMPT, --max_attempt MAX_ATTEMPT
                        Maximum attempt times.
  -a ALERT, --alert ALERT
                        The alert way, ["notification"|"email"], default is "notification"
  -u URL, --url URL     IFTTT webhook url
  -e TO_EMAIL, --to_email TO_EMAIL
                        The email destination for alerts.
  -b BEFORE_DATETIME, --before_datetime BEFORE_DATETIME
                        Only alert for an early appointment. For ex: 2021-11-15 20:27
```

- Use cron to launch jobs

```bash
0 7-22 * * * docker run -d --rm --name pprdv_instance --env-file /home/ubuntu/pprdv/.env flpvvvv/pprdv:latest python src/loop_check.py -w 60 -m 20 -b 2021-12-01 & sleep 2 && docker logs -f pprdv_instance > /home/ubuntu/pprdv.log
```

### V2.0

Changelog:

- Add check-first-appointment feature
- Dockerize codes
- Add arguments for `loop_check` function
- Build and push Docker Image to Docker Hub via GitlabCI


Example of program execution via Docker:

```bash
# .env file is needed for email sender credentials
docker run -it --rm --env-file .env flpvvvv/pprdv:latest python src/loop_check.py -w 30 -m 10 -e <email_to_receive_alert> -b 2021-12-01
```

`loop_check` function arguments:

```
usage: loop_check.py [-h] [-w WAIT_SECONDS] [-m MAX_ATTEMPT] [-e TO_EMAIL] [-b BEFORE_DATETIME]

Check if there is any appointment at the Préfecture.

optional arguments:
  -h, --help            show this help message and exit
  -w WAIT_SECONDS, --wait_seconds WAIT_SECONDS
                        Wait N seconds between each query.
  -m MAX_ATTEMPT, --max_attempt MAX_ATTEMPT
                        Maximum attempt times.
  -e TO_EMAIL, --to_email TO_EMAIL
                        The email destination for alerts.
  -b BEFORE_DATETIME, --before_datetime BEFORE_DATETIME
                        Only alert for an early appointment. For ex: 2021-11-15 20:27
```




### V1.0

Deployed to AWS EC2. Use `Python` to run the script.

Need to install **Chrome Headless** on EC2: [Ref](https://stackoverflow.com/questions/62848916/running-google-chrome-headless-on-ec2-ubuntu-server)

install pre-requsits

```
sudo apt update
sudo apt install unzip libnss3 python3-pip
```

install [driver for chrome 83](https://chromedriver.storage.googleapis.com/index.html?path=83.0.4103.39/)

```
cd /tmp/
sudo wget https://chromedriver.storage.googleapis.com/83.0.4103.39/chromedriver_linux64.zip
sudo unzip chromedriver_linux64.zip
sudo mv chromedriver /usr/bin/chromedriver
chromedriver --version
```

install google-chrome-stable current version

```
wget https://dl.google.com/linux/direct/google-chrome-stable_current_amd64.deb
sudo apt install ./google-chrome-stable_current_amd64.deb
```

check installation

```
google-chrome-stable --version
```




## Roadmap

- V4.0: Add FastAPI
- V5.0: Add Airflow

## License
MIT License
